#!/usr/bin/env python
# coding: utf-8

# In[3]:


import requests
import shutil
import os
import getpass
import arrow
import glob

import os
import arrow
import time
import subprocess
 


# In[13]:


#subprocess.call('ssh-add /home/pi/.ssh/piart', shell=True)


# In[ ]:





# In[4]:


def checkpathyear(blogpath):
    if arrow.now().strftime('%Y') in os.listdir('{}/galleries/'.format(blogpath)):
        return(True)
    else:
        return(False)


# In[5]:


def checkpathmonth(blogpath):
    if arrow.now().strftime('%m') in os.listdir('{}/galleries/{}'.format(blogpath, arrow.now().strftime('%Y'))):
        return(True)
    else:
        return(False)


# In[6]:


#def checkpathday(blogpath):
#    if arrow.now().strftime('%d') in os.listdir('{}/galleries/{}/{}'.format(blogpath, arrow.now().strftime('%Y'),
#                                                                arrow.now().strftime('%m'))):
#        return(True)
#    else:
#        return(False)


# In[7]:


def mkgalpath(blogpath):
    try:
        os.makedirs(blogpath + '/galleries/' + arrow.now().strftime('%Y') + '/' +  arrow.now().strftime('%m') + '/' + arrow.now().strftime('%d'))
    except:
        pass


# In[ ]:





# In[8]:


def mkpathmonth(blogpath):
    if checkpathmonth(blogpath) == False:
        os.mkdir(blogpath + '/galleries/' + arrow.now().strftime('%Y') + '/' +  arrow.now().strftime('%m'))
    else:
        pass


# In[9]:


#def mkpathyr(blogpath):
#    if checkpathyear(blogpath) == False:
#        os.mkdir(blogpath + '/galleries/' + arrow.now().strftime('%Y'))
#    else:
#        pass


# In[10]:


def mkpathyr(blogpath):
    try:
        os.makedirs(blogpath + '/galleries/' + arrow.now().strftime('%Y'))
    except:
        pass


# In[ ]:





# In[11]:


def todaygalleries():
    return(arrow.now().strftime('%Y/%m/%d'))


# In[12]:


#subprocess.call('scp -r {}/*.png {}'.format(imgorigin, fridpath), shell=True)


# In[13]:


def transferfiles(imgorigin, blogpath):
    #writes images to galleries folder. 
    subprocess.call('scp -r {}/*.png {}'.format(imgorigin, blogpath + '/galleries/' + todaygalleries()), shell=True)

    #todaygalleries()


# In[14]:


def checkpost(blogpath, blogpst):
    if blogpst + '.md' in os.listdir(blogpath + '/posts'):
        return(True)
    else:
        return(False)


# In[15]:


def returndatetime():
    #arnow = arrow.now().strftime
    return(arrow.now().strftime('%Y/%m/%d'))


# In[16]:


def returndate():
    #arnow = arrow.now().strftime
    return('/galleries/' + arrow.now().strftime('%Y/%m/%d'))


# In[17]:


def listdaysartwork(blogpath):
    return(os.listdir('{}/galleries/{}'.format(blogpath, returndatetime())))


# In[18]:


def createmeta(blogpath, blogpst, blogtag):
    with open(blogpath + '/posts/' + blogpst + '.meta', 'w') as daympo:
                daympo.write('.. title: {}\n.. slug: {}\n.. date: {}\n.. tags: {}\n.. link:\n.. description:\n.. type: text'.format(blogpst, blogpst, returndatetime(), blogtag)) 


# In[19]:


#createmeta('/home/pi/hackathonshack', 'flight', 'json2html, python')


# In[16]:


def createpost(blogpath, blogpst):
    with open(blogpath + '/posts/' + blogpst + '.md', 'w') as daymark:
            for toar in os.listdir(blogpath + returndate()):
                daymark.write('![{}]({}{})\n\n'.format(toar.replace('.png', ''), returndate(), '/' + toar))

    


# In[17]:


def createnewpost(blogpath, blogpst, txtcont):
    with open(blogpath + '/posts/' + blogpst + '.md', 'w') as daymark:
            daymark.write(txtcont)


            


# In[18]:


#foldergal = dict()


# In[1]:





# In[ ]:


def uploadpost(blogpath, ):
    defpath = request.args.get('blogpath')
    nameofblogpost = request.args.get('postname')
    #arg specify blog post
    subprocess.call('scp -r {}/posts/{}.* wcmckee@artctrl.me:/home/wcmckee/artctrltest/posts/'.format(defpath, nameofblogpost), shell=True)
    return('ok its alright')


# In[37]:


def createArtEntry(blogpath, blogpst, nameofart, comment):
    with open(blogpath + '/posts/' + blogpst + '.md', 'a+') as daymark:
            daymark.write('![{}]({})\n\n{}\n\n'.format(nameofart.replace('.png', ''), returndate() + '/' + nameofart, comment))

    


# In[6]:


def syncblog(blogpath, remoteserve, remoteath):
    '''give path to sync with username and server name,
    and remote path to sync to'''
    #arg specify blog post
    subprocess.call('rsync -rv {} {}:{}'.format(blogpath, remoteserve, remoteath), shell=True)
    return('ok its alright')
    


# In[12]:


#syncblog('/home/pi/artctrl/posts/', 'w@a.me', '/home/w/apitest/posts')


# In[19]:


#foldergal.update({'filename' : })


# In[16]:


#def galleriesmove()


# In[15]:


#mkgalpath('/home/pi/artctrl')


# In[14]:


#createmeta('/home/pi/git/artctrl.me-code', 'zhquiz', 'none')


# In[ ]:





# In[29]:


#transferfiles('/home/pi/Pictures/oct19mid', '/home/pi/artctrl')


# In[22]:


#returndate()


# In[3]:


#def listdaysartwork():
#    return(os.listdir('/home/pi/artctrl/galleries/{}'.format(returndate())))


# In[25]:


#listdaysartwork()


# In[ ]:





# In[23]:


#os.listdir('/home/pi/artctrl/galleries/{}'.format(returndate()))


# In[ ]:





# In[45]:


#createArtEntry('/home/pi/artctrl', 'oct19-portraits-landscapes', 'jd-landscape.png', 'A portrait. Random Joy Division quote at the time. I guess I was listening to some Joy Division and thinking about Wellington.')


# In[ ]:





# In[ ]:





# In[36]:


#createArtEntry('/home/pi/artctrl', 'oct19-portraits-landscapes', 'pillsbiruru.png', 'A portrait from RGD. This is of Pillsbiruru. I like her large glasses. They remind me of mine.')


# In[ ]:





# In[34]:


#createArtEntry('/home/pi/artctrl', 'oct19-portraits-landscapes', 'china-landscape', 'a landscape from imagination. chinese pingyin surrounding. It has been awhile since I have praticed.')


# In[ ]:





# In[ ]:





# In[ ]:


#jsondata = {'blogpath': '/home/pi/artctrl', 'imgorigin' : '/home/pi/oct199', 'postname' : 'joker-startwithnothing', 'posttags' : 'gimp, joker, ludumdare'}


# In[8]:


def commentart(nameofart, comment, blogdir, namepost):
    with open(blogdir + '/posts/' + namepost + '.md', 'a+') as blogwri:
        blogwri.write(nameofart + ' ' + comment)
    return({'art' : nameofart, 'comment' : comment, 'directory' : blogdir, 'namepost' : namepost})


# In[9]:


#commentart('person', 'it looks nice line', '/home/pi/artctrl', 'portraits')


# In[3]:


#for osgal in os.listdir('/home/pi/artctrl/galleries/' + todaygalleries()):
#    print(osgal)
#    artfeedback = input('comment on {}:'.format(osgal))
#    foldergal.update({osgal : dict({'comment' : artfeedback})})


# In[43]:


#type(foldergal)


# In[44]:


#foldergal.values()


# In[ ]:





# In[48]:


#for folgal in foldergal.values():
#    print(folgal)
    #print(folgal.key)


# In[49]:


#foldergal.values()


# In[34]:


#foldergal


# In[ ]:





# In[24]:


def mkdirs(path):
    os.makedirs(path + todaygalleries())


# In[26]:


#path + todaygalleries()


# In[ ]:





# In[25]:


#mkdirs('/home/pi/artctrl/galleries/')


# In[2]:


#"""A simple example of a hug API call with versioning"""
#import hug


#@hug.get('/echo', versions=1)
#def echo(text):
#    return text


#@hug.get('/echo', versions=range(2, 5))
#def echo(text):
#    return 'Echo: {text}'.format(**locals())


#@hug.get('/unversioned')
#def hello():
#    return 'Hello world!'


# In[ ]:


#def commentart()


# In[4]:


#from requests import post


# In[11]:


#jpayload = {'path' : '/home/pi/artctrl', 'post' : 'anexamplepost', 
#           'tag' : 'atag,another', 'imgorigin' : '/home/pi/Pictures/oct19mid'}


# In[ ]:





# In[ ]:





# In[20]:


#something = post('http://localhost:8000/', json = jpayload)


# In[21]:


#something.text


# In[ ]:





# In[ ]:




