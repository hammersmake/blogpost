#!/usr/bin/env python
# coding: utf-8

# In[1]:


import blogpost
from fastapi import FastAPI
from pydantic import BaseModel


# In[2]:


app = FastAPI(title="BlogPost API",
    description="cms api for creating blogposts",
    version="2")


# In[ ]:


class Mddetail(BaseModel):
    '''This is the markdown quick generation
    {'blogdir' : '/home/pi/artctrl', 'postname' : 'bookbookbook'''
    blogdir: str = '/home/pi/artctrl'
    postname: str = 'bookbookbook'


# In[3]:


class Postdetail(BaseModel):
    '''These are the template meta data. It makes up the 
    structure of the post.
    Example:
    {'blogdir' : '/home/pi/artctrl', 'postname' : 'a monday new post',
    'postag' : 'gimp, drawing', 'imgorigin' : '/home/pi/testdir'}'''
    blogdir: str = '/home/pi/artctrl'
    postname: str = 'anewpost'
    postag: str = 'gimp, drawing'
    imgorigin: str = '/home/pi/testdir'

class Artdetail(BaseModel):
    '''These are the images meta data
    nameofart, comment. 
    {'blogdir' : '/home/pi/artctrl', 'postname' : 'bookbookbook',
    'nameofart' : 'image.png', 
    'comment' : 'This was image on desktop.'}'''
    blogdir: str = '/home/pi/artctrl'
    postname: str = 'bookbookbook'
    nameofart: str = 'hammersmake.png'
    comment: str = 'This is a digital drawing that was completed during the day.'
#postdet.blogdir, postdet.postname, 
#artdetail.nameofart, artdetail.comment   
    


# In[4]:


class Item(BaseModel):
    name: str
    description: str = None
    price: float
    tax: float = None

@app.post("/items/")
async def create_item(item: Item):
    return item


# In[5]:


#some = Item.dict


# In[6]:


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


# In[7]:


#@app.get('/blogpost')
#async def getblog(blogdir: str, postname: str):
#    '''return a blog post from a blog'''
#    explorers.flyoneway(cityfrom, citytoarrive, datefrom, dateto)
#    explorers.flyight2jsn(blogdir, postname, cityfrom, citytoarrive, datefrom, dateto)
#    return('ok')


# In[8]:


@app.post('/setbp')
async def setupost(postdet : Postdetail):
    '''Create meta and galleries transfer'''
    blogpost.mkgalpath(postdet.blogdir)
    blogpost.createmeta(postdet.blogdir, postdet.postname, 
                       postdet.postag)
    blogpost.transferfiles(postdet.imgorigin, postdet.blogdir)
    return('ok')


# In[ ]:





# In[ ]:


@app.post('/setfullmd')
async def setfullmd(mddetail :  Mddetail):
    #blogdir: str = '/home/pi/artctrl'
    #postname
    blogpost.createpost(mddetail.blogdir, mddetail)
    return('ok')


# In[9]:


@app.post('/dobp')
async def dopost(artdetail : Artdetail):
    '''Make an entry to blogpost. Create a entry into the post
    containing filename of image and text that will be below 
    the image.'''
    blogpost.createArtEntry(artdetail.blogdir, artdetail.postname, 
                            artdetail.nameofart, artdetail.comment)
    return('ok')


# In[10]:


@app.post('/syncbp')
async def syncpost(postdet : Postdetail):
    '''Sync a blogpostto remote server via rsync'''
    blogpost.syncblog(postdet.blogdir + '/posts/', 'wcmckee@artctrl.me',
                     '/home/wcmckee/apitest/p')
    return('ok')


# In[12]:


#import subprocess


# In[ ]:





# In[15]:


#subprocess.call('eval `ssh-agent -s`&ssh-add /home/pi/.ssh/piart', shell=True)


# In[22]:


#subprocess.call('rsync -rv {} {}:{}'.format('/home/pi/a/', 'w@a.me', 
#                                            '/home/w/apitest/p'), shell=True)


# In[16]:


#blogpost.syncblog('/home/pi/a/', 'w@a.me',
#                     '/home/w/p')


# In[ ]:





# In[ ]:


@app.post('/syncgals')
async def syncgals(postdet : Postdetail):
    '''Sync a galleries to remote server via rsync'''
    blogpost.syncblog(postdet.blogdir + '/galleries/', 'wcmckee@artctrl.me',
                     '/home/wcmckee/apitest/galleries')
    return('ok')


# In[ ]:


#"""blogpost api"""
##import hug
#def cors_support(response, *args, **kwargs):
#    response.set_header('Access-Control-Allow-Origin', '*')

#@hug.get('/', examples='city=singapore&filtertype=hotels', requires=cors_support)
#def mercity(city: hug.types.text, filtertype: hug.types.text):
#    """Returns city guide for city"""
    #explorers.merchantscity('singapore', 'hotels')
#    return explorers.merchantscity(city, filtertype)


# In[ ]:


#@hug.get('/', examples='city=singapore&filtertype=hotels', requires=cors_support)
#def mercity(city: hug.types.text, filtertype: hug.types.text):
#    """Returns city guide for city"""
    #explorers.merchantscity('singapore', 'hotels')
#    return explorers.merchantscity(city, filtertype)


# In[4]:


#from flask import Flask, request, jsonify, render_template
#from flask_restful import reqparse, abort, Resource, Api


# In[5]:


#app = Flask(__name__)
#api = Api(app)


# In[3]:


#class CreatePost(Resource):
#    def get(self):
       
#        jsondata = request.get_json(force=True)
#        blogpath = jsondata['blogpath']
#        postname = jsondata['postname']
#        with open(blogpath + '/posts/' + postname + '.meta', 'r') as pomet:
#            return pomet.read(), 201
        
        
#    def post(self):
#        jsondata = request.get_json(force=True)
##        blogpath = jsondata['blogpath']
#        imgorigin = jsondata['imgorigin']
#        postname = jsondata['postname']
#        posttags = jsondata['posttags']
        #lenth = request.args.get('length')
        #ic.createdbtext(nameofdb)
#        blogpost.mkpathyr(blogpath)
#        blogpost.mkpathmonth(blogpath)
#        blogpost.mkpathday(blogpath)
#        blogpost.transferfiles(imgorigin, blogpath)
#        blogpost.createmeta(blogpath, postname, posttags)
#        blogpost.createpost(blogpath, postname)
#        return postname + ' created successfully', 201


# In[6]:


#jsondata = dict({'blogpath': '/home/pi/artctrl', 'imgorigin' : })


# In[1]:


#jsondata = {'blogpath': '/home/pi/artctrl', 'imgorigin' : '/home/pi/Documents/cronulla/july19xronulla', 'postname' : 'hurstville-cronulla', 'posttags' : 'gimp, sydney, landscape'}


# In[10]:


#jsondata['blogpath']


# In[ ]:





# In[ ]:





# In[6]:



#blogpath = jsondata['blogpath']
#imgorigin = jsondata['imgorigin']
#postname = jsondata['postname']
#posttags = jsondata['posttags']
#lenth = request.args.get('length')
#ic.createdbtext(nameofdb)
#blogpost.mkpathyr(blogpath)
#blogpost.mkpathmonth(blogpath)
#blogpost.mkpathday(blogpath)
#blogpost.transferfiles(imgorigin, blogpath)
#blogpost.createmeta(blogpath, postname, posttags)
#blogpost.createpost(blogpath, postname)
#print(postname + ' created successfully')


# In[4]:


#class WritePost(Resource):
#    def post(self):
#        jsondata = request.get_json
#        blogpath = request.args.get('blogpath')
#        postname = request.args.get('postname')
#        txt = request.args.get('txt')
#        posttags = request.args.get('posttags')
#        blogpost.createmeta(blogpath, postname, posttags)
#        blogpost.createnewpost(blogpath, postname, txt)
#        return(postname + ' created successfully')


# In[5]:


#api.add_resource(CreatePost, '/createpost')
#api.add_resource(WritePost, '/writepost')

#if __name__ == '__main__':
#    app.run(debug=True)


# In[ ]:




