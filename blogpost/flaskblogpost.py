#!/usr/bin/env python
# coding: utf-8

# In[1]:


import blogpost


# In[2]:


from flask import Flask, request, jsonify, render_template
from flask_restful import reqparse, abort, Resource, Api


# In[ ]:


app = Flask(__name__)
api = Api(app)


# In[3]:


class CreatePost(Resource):
    def get(self):
       
        jsondata = request.get_json(force=True)
        blogpath = jsondata['blogpath']
        postname = jsondata['postname']
        with open(blogpath + '/posts/' + postname + '.meta', 'r') as pomet:
            return pomet.read(), 201
        
        
    def post(self):
        jsondata = request.get_json(force=True)
        blogpath = jsondata['blogpath']
        imgorigin = jsondata['imgorigin']
        postname = jsondata['postname']
        posttags = jsondata['posttags']
        #lenth = request.args.get('length')
        #ic.createdbtext(nameofdb)
        blogpost.mkpathyr(blogpath)
        blogpost.mkpathmonth(blogpath)
        blogpost.mkpathday(blogpath)
        blogpost.transferfiles(imgorigin, blogpath)
        blogpost.createmeta(blogpath, postname, posttags)
        blogpost.createpost(blogpath, postname)
        return postname + ' created successfully', 201


# In[4]:


class WritePost(Resource):
    def post(self):
        jsondata = request.get_json
        blogpath = request.args.get('blogpath')
        postname = request.args.get('postname')
        txt = request.args.get('txt')
        posttags = request.args.get('posttags')
        blogpost.createmeta(blogpath, postname, posttags)
        blogpost.createnewpost(blogpath, postname, txt)
        return(postname + ' created successfully')


# In[5]:


api.add_resource(CreatePost, '/createpost')
api.add_resource(WritePost, '/writepost')

if __name__ == '__main__':
    app.run(debug=True)


# In[ ]:




