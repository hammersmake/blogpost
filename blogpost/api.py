"""A simple example of a hug API call with versioning"""
import hug
import blogpost

#@hug.get('/')
#def bp():
#    return blogpost
#createArtEntry(blogpath, blogpst, nameofart, comment)

@hug.post('/')
def bpg(path:hug.types.text, post:hug.types.text, tag:hug.types.text, imgorigin:hug.types.text):
    """Create a blogpost meta data"""
    blogpost.mkgalpath(path)
    blogpost.createmeta(path, post, tag)
    blogpost.transferfiles(imgorigin, path)
    return(blogpost.listdaysartwork(path))

#return {'message': 'Happy {0} Birthday {1}!'.format(age, name),
#            'took': float(hug_timer)} 
